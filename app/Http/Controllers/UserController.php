<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use DB;


class UserController extends Controller
{
        public function index()
        {
            if(Auth::user()) {
                $user = Auth::user();
                $data['users'] = $user;
                $data['sideaktif'] = 1;
                $data['file'] = 0;
                echo "wow1";
                // return view('dashboard',$data);
            }
            return redirect('/login');    
        
        }

        public function loginForm()
        {
            if(Auth::user()) {
                return redirect('/');
            }
            return view('admin.login');
        }

        public function login(Request $request)
        {
            $validated = $request->validate([
                'username' => 'required',
                'password' => 'required'
            ]);
            $user = User::where('username', $request->username)->first();
            if($user->password == md5($request->password)) {
                Auth::login($user);
                return redirect('/');
            }
            return redirect('/login');
        }

        public function logout()
        {
            Auth::logout();
            return redirect('/login');
        }

}
