<!DOCTYPE html>
<html>

	<head>

		<title>rb.singaraja</title>

		
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta content="IE=edge" http-equiv="X-UA-Compatible">
		<meta name="x-apple-disable-message-reformatting">
		<meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">

		<!-- <link rel="stylesheet" type="text/css" href="style/fonts/style.css"> -->
		<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
		<!-- CSS only -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
		<link rel="stylesheet" href="{{asset('css/bootstrap-4.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/owlcarousel/owl.carousel.min.css')}}">
		<link rel="stylesheet" href="{{asset('css/owlcarousel/owl.theme.default.css')}}">
		<link rel="stylesheet" href="{{asset('css/owlcarousel/owl.theme.default.css')}}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  		<link rel="stylesheet" href="{{asset('css/style.css')}}">
  		<link rel="stylesheet" href="{{asset('css/pelayanan.css')}}">
	</head>

	<body>

		@include('components.navbar')
		<main class="content-wrapper">
			@yield('content')
		</main>	
		@include('components.footer')
		<!-- jQuery -->
		<script src="{{asset('js/jquery/jquery.min.js')}}"></script>

		<!-- bootstrap -->
		<script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>

		<script src="{{asset('js/owlcarousel/owl.carousel.min.js')}}"></script>
		<script>

			$(document).ready(function(){
				$(".owl-pelayanan-birokrasi").owlCarousel({
					loop:true,
					margin:10,
					nav:true,
					dots:false,
					responsive:{
						0:{
							items:2
						},
						600:{
							items:4
						},
						1000:{
							items:4
						}
					}
				});
				$(".owl-carousel").owlCarousel({
					loop:true,
					nav:true,
					items:1,
					navigation: true,
					smartSpeed:450
				});
			});
			

			const $body = $('body');
			const $menuOpen = $('.menu-toggle');
			const $menuClose = $('.cancel-btn');
			const $btnSearch = $('.search-toggle');
			$(function () {
			    $menuOpen.click(function() {
			        $body.addClass('menu-open');   
			    });
			    $menuClose.click(function() {
			        $body.removeClass('menu-open');
			    });

				$btnSearch.click(function() {
					$body.toggleClass('search-open');
				});

			});
		</script>
	</body>
	
</html>