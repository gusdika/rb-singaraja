	
@extends('layouts.main')

<!-- @section('title' , 'Detail Jabatan') -->

@section('content')
	<!-- Set up your HTML -->
	<div class="td-trending-now-wrapper">
				<div class="td-trending-now-title">Layanan Publik</div>
				<div class="owl-carousel trending-news-slideVertical">
					<div class="td-trending-now-display-area"><a href="">Patricia Urquiola Coats Transparent Glas Tables for Livings</a></div>
					<div class="td-trending-now-display-area"><a href="">Ambrose Seeks Offers on Downtown Building for Apartments</a></div>
					<div class="td-trending-now-display-area"><a href="">Another Big Apartment Project Slated for Broad Ripple Company</a></div>
				</div>
			</div>
			
			<div class="kgUHa-D row">
					<div class="gBfXnH col-lg-8 col-md-8 col-sm-8 col-xs-12">
						<div class="owl-carousel slideshow-listHorizontal">
							@for ($i = 0; $i < 10; $i++)
							<div>
									<div class="media media--text-overlay block-link">
										<div class="media__image">
											<a  href="https://news.detik.com/berita/d-5571475/langgar-protokol-kesehatan-waterpark-di-enrekang-sulsel-ditutup?tag_from=wp_hl_img" class="media__link">
												<span class="ratiobox ratiobox--16-9 lqd" style="background-image: url(&quot;https://akcdn.detik.net.id/community/media/visual/2021/05/17/kapolres-enrekang-akbp-andi-sinjaya-mengecek-penerapan-protokol-kesehatan-di-tempat-wisata_169.jpeg?w=700&amp;q=90&quot;);">
													<img src="https://bulelengkab.go.id/upload/konten/97-pemkab-buleleng-kucurkan-dana-16-m-tuntaskan-pembangunan-rth-taman-bung-karno.jpg" alt="West Brom Vs Liverpool: Gol Alisson Bawa The Reds Menang Dramatis" title="West Brom Vs Liverpool: Gol Alisson Bawa The Reds Menang Dramatis" class="" > 
												</span>
											</a>
										</div>
										<div class="media__text">
											<h2 class="media__title">
												<a href="https://news.detik.com/berita/d-5571475/langgar-protokol-kesehatan-waterpark-di-enrekang-sulsel-ditutup?tag_from=wp_hl_judul" class="media__link">Pemkab Buleleng Kucurkan Dana 16 M Tuntaskan Pembangunan RTH Taman Bung Karno</a>
											</h2>
											<div class="media__date mgt-4">singarahaNews | <span d-time="1621185021" title="Senin, 17 Mei  2021 00:10 WIB">23 menit yang lalu</span></div>
										</div>
									</div>
									<div class="headline-terkait">
										<div class="headline-terkait__title">Berita Terkait</div>
										<div class="list-content list-content--column grid-row">
												<article class="list-content__item column">
													<h3 class="list-content__title">
														<a onclick="_pt(this, &quot;headline&quot;, &quot;Polisi Identifikasi Wanita Maki Petugas Hendak ke Anyer: Segera Diamankan!&quot;, &quot;berita terkait 1&quot;)" href="https://news.detik.com/berita/d-5571423/polisi-identifikasi-wanita-maki-petugas-hendak-ke-anyer-segera-diamankan?tag_from=wp_hl_terkait">Polisi Identifikasi Wanita Maki Petugas Hendak ke Anyer: Segera Diamankan!</a>
													</h3>
												</article>
																<article class="list-content__item column">
													<h3 class="list-content__title">
														<a onclick="_pt(this, &quot;headline&quot;, &quot;Cek Penutupan Objek Wisata, Kapolresta Mataram Pastikan Aman &amp; Lancar&quot;, &quot;berita terkait 2&quot;)" href="https://news.detik.com/berita/d-5571396/cek-penutupan-objek-wisata-kapolresta-mataram-pastikan-aman--lancar?tag_from=wp_hl_terkait">Cek Penutupan Objek Wisata, Kapolresta Mataram Pastikan Aman &amp; Lancar</a>
													</h3>
												</article>
										</div>
									</div>
							</div>
							@endfor
						</div>
					</div>
					<div class="dkOPyg col-lg-4 col-md-4 col-sm-4 col-xs-12">
						<div class="Viewweb__StyledView-p5eu6e-0 jqDfEK">
							<div class="Viewweb__StyledView-p5eu6e-0 gkyRKb Headingweb__Wrapper-sc-1k439k1-0 VvtAP">
								<a class="LabelLinkweb-njxxwb-0 cJvuwm" href="/trending"><span class="Textweb__StyledText-sc-1fa9e8r-0 jLsJGy">Trending</span></a>
								<a class="LabelLinkweb-njxxwb-0 cJvuwm" href="/trending">
									<button data-qa-id="ft-loadmore" class="Buttonweb__ButtonWrapper-sc-11sswl3-0 exjjVR">
										<div class="Viewweb__StyledView-p5eu6e-0 drBeOv">
											<div class="Viewweb__StyledView-p5eu6e-0 htSlBT">
												<img name="chevron-next-black icon" data-qa-id="chevron-next-black" src="{{asset('img/menu-vertical.svg')}}" alt="chevron-next-black" class="Iconweb__StyledIcon-qahpco-0 cKFCVl">
											</div>
											<div class="Viewweb__StyledView-p5eu6e-0 jJCycy">
												<span class="Textweb__StyledText-sc-1fa9e8r-0 ekPWUQ">Lihat lainnya</span>
											</div>
										</div>
									</button>
								</a>
							</div>
							<div width="100%" height="1px" class="Viewweb__StyledView-p5eu6e-0 gRorUC"></div>
						</div>
						<div height="553px" class="Viewweb__StyledView-p5eu6e-0 ejgikD">
							<div class="Listweb__Scroll-ra4f1y-0 blRxOX">
								@for ($i = 0; $i < 10; $i++)
								<div class="Viewweb__StyledView-p5eu6e-0 kLvCSr"> <!-- start card -->
									<div style="width: 100%;">
										<div style="width: 100%;">
											<div data-qa-id="trending-story-item" class="Viewweb__StyledView-p5eu6e-0 bSbCiV">
												<div class="Viewweb__StyledView-p5eu6e-0 kcaV">
													<a class="LabelLinkweb-njxxwb-0 cJvuwm" href="/gosip-artis/ivan-gunawan-ingin-minta-maaf-balasan-deddy-corbuzier-malah-bikin-kesel-1vkslwIR1Hx">
														<span class="Textweb__StyledText-sc-1fa9e8r-0 jeegbA CardContentweb__CustomText-sc-1wr516g-0 hpLohH" data-qa-id="title">Sosialisasi dan persiapan pelaksanaan kopetensi inovasi pelayanan publik tahun 2021</span>
														<div class="Viewweb__StyledView-p5eu6e-0 dZTDGp">
															<div class="Viewweb__StyledView-p5eu6e-0 jqDfEK">
																<div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
																	<div class="Viewweb__StyledView-p5eu6e-0 enSBkx">
																		<span class="LabelLinkweb-njxxwb-0 bHjxWK">
																			<span class="LabelLinkweb-njxxwb-0 bHjxWK">
																				<div data-qa-id="avatar" class="Avatarweb__AvatarWrapper-ftedd9-0 iDBAqk">
																					<div color="#FFFFFF" class="Avatarweb__AvatarImageContainer-ftedd9-1 ffynyp">
																						<img src="https://bulelengkab.go.id/assets/front_end/icon_desa/alasangker.png" style="width: 101%;height: 101%;">
																					</div>
																				</div>
																			</span>
																		</span>
																	</div>
																	<div class="Viewweb__StyledView-p5eu6e-0 ixPpS">
																		<div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
																			<div class="Viewweb__StyledView-p5eu6e-0 bomQmO">
																				<span class="LabelLinkweb-njxxwb-0 bHjxWK">
																					<div color="gray70" class="TextBoxweb__StyledTextBox-sc-1wzqkk1-0 jwxGoF">
																						<span class="Textweb__StyledText-sc-1fa9e8r-0 iqORTp CardContentweb__NameText-sc-1wr516g-1 cnIZYX" data-qa-id="author-name" style="word-break: break-word; overflow-wrap: break-word;">Info Singaraja</span>
																					</div>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</a>
													
													<div class="Viewweb__StyledView-p5eu6e-0 jmNTGy">
														<div class="Viewweb__StyledView-p5eu6e-0 hbSMcY">
															<button aria-label="like-button" data-qa-id="like-button-wrapper" class="NewsCardFooterweb__ActionButton-sc-1140nk0-1 lghqze">
																<img name="like icon" data-qa-id="like" src="{{asset('img/love.svg')}}" alt="like" class="Iconweb__StyledIcon-qahpco-0 bChDNn">
																<span class="Textweb__StyledText-sc-1fa9e8r-0 exBPjh">0</span>
															</button>
																<a data-qa-id="comment-button-wrapper" class="LabelLinkweb-njxxwb-0 cJvuwm NewsCardFooterweb__ActionLink-sc-1140nk0-2 iVVnGn" href="/gosip-artis/response/ivan-gunawan-ingin-minta-maaf-balasan-deddy-corbuzier-malah-bikin-kesel-1vkslwIR1Hx">
																<img name="comment icon" data-qa-id="comment" src="{{asset('img/chat.svg')}}" alt="comment" class="Iconweb__StyledIcon-qahpco-0 bChDNn">
																<span class="Textweb__StyledText-sc-1fa9e8r-0 exBPjh">0</span>
															</a>
															<span class="Textweb__StyledText-sc-1fa9e8r-0 exBPjh"> 6 jam</span>
														</div>
														<button aria-label="more-button" data-qa-id="more-button-wrapper" class="NewsCardFooterweb__ActionButton-sc-1140nk0-1 dqpESP">
															<img name="more-vertical icon" data-qa-id="more-vertical" src="{{asset('img/menu-vertical.svg')}}" alt="more-vertical" class="Iconweb__StyledIcon-qahpco-0 bChDNn NewsCardFooterweb__ActionIcon-sc-1140nk0-0 cDnjUS">
														</button>
													</div>
												</div>
												<div class="Viewweb__StyledView-p5eu6e-0 jxsxmf">
													<a class="LabelLinkweb-njxxwb-0 cJvuwm" href="/gosip-artis/ivan-gunawan-ingin-minta-maaf-balasan-deddy-corbuzier-malah-bikin-kesel-1vkslwIR1Hx">
													<div data-qa-id="image" class="Thumbnailweb__ThumbnailContainer-sc-15nqo19-2 jxacmi">
														<div>
															<div height="100%" width="80px" alt="photo_2021-05-16_14-29-32.jpg" class="Imageweb__ImageWrapper-sc-1kvvof-0 eZCymS" style="background: url(&quot;https://rb.baliprov.go.id/wp-content/uploads/2021/04/b-150x150.jpg&quot;) center center / cover repeat;"></div>
															<noscript></noscript>
														</div>
													</div>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div> <!-- end -->
								<div class="Viewweb__StyledView-p5eu6e-0 gdMcah"><div width="100%" height="1px" class="Viewweb__StyledView-p5eu6e-0 gRorUC"></div></div>
								@endfor
								
							</div>
						</div>
						
				</div>
			</div>
			@include('common.pelayanan')
			@include('common.birokrasi')

@endsection